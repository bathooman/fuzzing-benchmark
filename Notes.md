ecc_ec_mult(ecc_g_point_x, ecc_g_point_y, priv_key, pub_x, pub_y); -> ecc.h:58
  ecc_gen_pub_key(priv, pub_x, pub_y); -> dtls-crypto.c:384
    dtls_ecdsa_generate_key(config->keyx.ecdsa.own_eph_priv, ephemeral_pub_x, ephemeral_pub_y, DTLS_EC_KEY_SIZE); -> dtls.c:2050
      dtls_send_server_key_exchange_ecdh(ctx, peer, ecdsa_key); -> dtls.c:2188
        dtls_send_server_hello_msgs(ctx, peer); -> dtls.c:3470
          handle_handshake_msg(ctx, peer, session, role, state, data, data_length); -> dtls.c:3555
            handle_handshake(ctx, peer, session, role, state, data, data_length); -> dtls.c:3892
              dtls_handle_message(ctx, &session, buf, len); -> dtls-fuzz.c:260




  uint32_t priv[8]= {16843009, 16843009, 16843009, 16843009, 16843009, 16843009, 16843009, 16843009};
  uint32_t pub_x[8] = {3289402, 0, 4214999, 0, 12, 20, 15, 3};
  uint32_t pub_y[8] = {1585920012, 0, 340294, 0, 808595506, 758394925, 824193840, 808598069}

  const uint32_t ecc_g_point_x[8] = {3633889942, 4104206661, 770388896, 1996717441, 1671708914, 4173129445, 3777774151, 1796723186}
  const uint32_t ecc_g_point_y[8] = {935285237, 3417718888, 1798397646, 734933847, 2081398294, 2397563722, 4263149467, 1340293858}
