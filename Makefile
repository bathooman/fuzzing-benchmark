
CFLAGS := -Wall
LDFLAGS := -lcrypto
CC := gcc

bench: bench.o
	$(CC) bench.o -o bench $(LDFLAGS)

bench.o: bench.c
	$(CC) $(CFLAGS) -c bench.c 

clean: 
	rm -rf bench.o

