#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "openssl/sha.h"
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>

#define ISSYM 0

#if ISSYM
#include klee/klee.h
#endif

#define EI_NIDENT	16
#define MAGIC ">!n"

bool Sentinel = 0;
int checkbinary(int fd)
{
  unsigned char	e_ident[EI_NIDENT];
  char ExMagic[4];
  if(read(fd, e_ident, EI_NIDENT) == -1)
  {
    printf("Error reading file");
    return -1;
  }
  memcpy(ExMagic, &e_ident[1],3);
  ExMagic[3] = '\0';

  if (strcmp(ExMagic, MAGIC) == 0)
    assert(0 && "checkbinary function (using strcmp)");
  else
  {
    printf("The execution of function [checkbinary] has been finished successfully.\n" );
    return 0;
  }
}

int checkhash(int fd)
{
  unsigned char	e_ident[EI_NIDENT];
  char ExMagic[4];

  if(read(fd, e_ident, EI_NIDENT) == -1)
  {
    printf("Error reading file");
    return -1;
  }
  memcpy(ExMagic, &e_ident[1],3);
  ExMagic[3] = '\0';


  unsigned char secret_hash[SHA_DIGEST_LENGTH];
  size_t secret_length = strlen(MAGIC);
  SHA1(MAGIC, secret_length, secret_hash);

  size_t length = strlen(ExMagic);
  unsigned char input_hash[SHA_DIGEST_LENGTH];
  SHA1(ExMagic, length, input_hash);

  if (memcmp(input_hash, secret_hash, SHA_DIGEST_LENGTH) == 0)
    assert(0 && "checkhash function (using memcmp and SHA1)");
  else
  {
    printf("The execution of function [checkhash] has been finished successfully.\n" );
    return 0;
  }

}

int implicit (int fd)
{
  int count = 1;
  unsigned char c[1];

  while (read(fd, c, sizeof(char)) != 0)
  {
    if (count == 10)
    {
      int val = (int)c[0];
      if (val == 90)
      {
        Sentinel = 1;
      }
    }
    if (count == 25)
    {
      int val = (int)c[0];
      if(val == 122 && Sentinel == 1)
      {
        assert(0 && "implicit function");
      }
    }
    count++;
  }
  printf("The execution of function [implicit] has been finished successfully.\n" );
}


int expo (int inp)
{
  return inp * inp;
}

int optimistic_solving (int val)
{
  #if ISSYM
  klee_make_symbolic(&val, sizeof(val), "val");
  #endif
  val = val + 4;
  val = expo(val);
  printf("%d\n",val );
  if (val >= 20 && val < 81)
  {
    printf("Modified value is between 20 and 90\n");
  }
  if (val == 81)
  {
    assert(0 && "Modifying the input value & optimistic solving");
  }
  printf("The execution of function [optimistic_solving] has been finished successfully.\n" );
}
int symbolic_condition(int val)
{
  #if ISSYM
  klee_make_symbolic(&val, sizeof(val), "val");
  #endif
  int temp = 500;

  while (1) {
    temp++;
    if (temp == 518)
      assert(0 && "Symbolic internal loop condition");
    val--;
    if (val <= 0) break;
  }

  if (temp == 700)
    assert(0 && "Symbolic external loop condition");

  printf("The execution of function [symbolic_condition] has been finished successfully.\n" );
  return 0;

}

void print_Usage(const char *argv)
{
  printf("Usage: %s [-i FILENAME] [-m] [-s] [-p] [-o] [-l] \n", argv);
  exit(2);
}


int main(int argc, char *argv[]) {

  int iarg = 0;
  char fileaddress[100];
  int marg = 0;
  int sarg = 0;
  int parg = 0;
  int oarg = 0;
  int larg = 0;


  int opt;
  int fd; //file descriptor for
  int inputval = 0;
  char inp[4];


  while ((opt = getopt(argc, argv, "i:mspol")) != -1) {
    switch (opt) {
      case 'i':
        iarg = 1;
        fd = open(optarg, O_RDONLY);
        break;
      case 'm':
        marg = 1;
        break;
      case 's':
        sarg = 1;
        break;
      case 'p':
        parg = 1;
        break;
      case 'o':
        oarg = 1;
        break;
      case 'l':
        larg = 1;
        break;
      default:
        fprintf(stderr, "Usage: %s [-i FILENAME] [-m] [-s] [-p] [-o] [-l] \n", argv[0]);
        exit(EXIT_FAILURE);
    }
  }

  if (iarg == 1)
  {

    if (fd == -1)
      assert(0 && "Failure of open syscall");
    if (marg == 1)
      checkbinary(fd);
    if (sarg == 1)
      checkhash(fd);
    if (parg == 1)
      implicit(fd);
    if (oarg == 1)
    {
      read(fd, inp, sizeof(int));
      inputval = atoi(inp);
      optimistic_solving(inputval);
    }
    if (larg == 1)
    {
      read(fd, inp, sizeof(int));
      inputval = atoi(inp);
      symbolic_condition(inputval);
    }
    if (marg == 0 && sarg == 0 && parg == 0 && oarg == 0 && larg == 0)
      print_Usage(argv[0]);
  }
  else
  {
    print_Usage(argv[0]);
  }

  return 0;
}
