
#include "ecc/ecc.h"
#include <stdio.h>
#include <klee/klee.h>
#include <time.h>

int mult(int val)
{
    klee_make_symbolic(&val, sizeof(val), "val");
    val = val * 2;
    val = val + 1357;
    if (val < 2500)
    {
        printf("hello");
    }
    else
    {
        printf("bye");
    }
    
    return val;
}

int main ()
{
  uint32_t priv[8]= {16843009, 16843009, 16843009, 16843009, 16843009, 16843009, 16843009, 16843009};
  uint32_t pub_x[8] = {3289402, 0, 4214999, 0, 12, 20, 15, 3};
  uint32_t pub_y[8] = {1585920012, 0, 340294, 0, 808595506, 758394925, 824193840, 808598069};

  ecc_gen_pub_key(priv, pub_x, pub_y);

  for (int i=0 ; i < 8 ; i++)
  {
      printf("%u:",priv[i]);
  }
  printf("\npubx:\n"); 
  for (int i=0 ; i < 8 ; i++)
  {
      printf("%u:",pub_x[i]);
  }
  printf("\npuby:\n");
  for (int i=0 ; i < 8 ; i++)
  {
      printf("%u:",pub_y[i]);
  }
  printf("\n");
  int x = mult(22); 
  printf("%d", x);

}
